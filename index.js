const amqp = require('amqplib/callback_api')
const bluebird = require('bluebird')
const safeStringify = require('fast-safe-stringify')
const { customLogBack } = require('@iperlink/error_logger')
const jwt = require('jsonwebtoken')
const _ = require('lodash')
const Redis = require('ioredis')

const REDISHOST = process.env.REDISHOST || 'localhost'
const redis = new Redis({ host: REDISHOST })

const { host } = require('./config')

const connectPromise = (queue, cb) => {
  amqp.connect(`amqp://${host}`, (err, conn) => {
    if (err) {
      const errLog = customLogBack({
        err: 'no connection to rabitt queue',
      }, true)
      cb(errLog)
    } else {
      try {
        const from = jwt.sign({ name: queue }, process.env.api_key)
        conn.createChannel((error, ch) => {
          cb(error, { ch, conn, from, queue })
        })
      } catch (err2) {
        const errLog = customLogBack({
          err: `unsigned jwt at ${queue}`,
        }, true)
        cb(errLog)
      }
    }
  })
}


const connect = bluebird.promisify(connectPromise)

const send = (conn, msg) => {
  const { ch, queue, from } = conn
  ch.assertQueue(queue, { durable: true })
  const sendMsg = safeStringify({ ...msg, from })
  ch.sendToQueue(queue, new Buffer(`${sendMsg}`))
}

const pub = (conn, msg) => {
  const { ch, queue } = conn
  ch.assertExchange(queue, 'fanout', { durable: false })
  ch.publish(queue, '', new Buffer(`${msg}`))
}


const listen = async (conn, name, fn, params) => {
  const key = process.env.api_key
  const cont = await redis.get('getServices')
  const { ch, queue } = conn
  ch.prefetch(1)
  ch.assertQueue(queue, { durable: true })
  const definition = JSON.parse(cont)[name]
  ch.consume(queue, msg => {
    try {
      const parsed = JSON.parse(msg.content.toString())
      const decoded = jwt.verify(parsed.from, key)
      const auth = (definition.authorized.includes(decoded.name))
      if (auth) {
        const pass = _.omit(parsed, 'from')
        const passed = { ...params, ...pass }
        fn(passed)
      } else {
        customLogBack({
          err: `unauthorized access from ${name} at ${queue}`,
        }, true)
      }
      ch.ack(msg)
    } catch (error) {
      ch.ack(msg)
    }
  })
}

const sub = (conn, fn, params) => {
  const { ch, queue } = conn
  ch.assertExchange(queue, 'fanout', { durable: false })
  ch.assertQueue('', { exclusive: true }, (err, q) => {
    ch.bindQueue(q.queue, queue, '')
    ch.consume(q.queue, msg => {
      const parsed = msg.content.toString()
      const passed = { ...params, parsed }
      fn(passed)
      ch.ack(msg)
    })
  })
}

const connAndSend = async (queue, msg) => {
  const conn = await connect(queue)
  await send(conn, msg)
  setTimeout(() => {
    conn.conn.close()
  }, 1500)
}

const connAndPub = async (queue, msg) => {
  const conn = await connect(queue)
  await pub(conn, msg)
  setTimeout(() => {
    conn.conn.close()
  }, 1500)
}

const connAndListen = async (queue, fn, key) => {
  try {
    const conn = await connect(queue)
    listen(conn, queue, fn, key)
  } catch (err) {
    console.log(err)
  }
}

const connAndSub = async (queue, msg, fn) => {
  const conn = await connect(queue)
  sub(conn, msg, fn)
}

const getQueryMsg = msg => {
  const parsed = msg.parsed
  return JSON.parse(parsed)
}

const sendIntegration = queue => {
  if (process.env.test === 'integration') {
    connAndSend(queue, safeStringify(''))
  }
}

exports.brokerConnAndSend = connAndSend
exports.brokerConnAndPub = connAndPub
exports.brokerConnect = connect
exports.brokerSend = send
exports.brokerListen = connAndListen
exports.brokerSub = connAndSub
exports.getQueryMsg = getQueryMsg
exports.sendIntegration = sendIntegration
