const { brokerConnect, brokerSend, brokerConnAndSend } = require('../index')
const safeStringify = require('fast-safe-stringify')


const connectAsync = async queue => {
  const conn = await brokerConnect(queue)
  return conn
}

const sendMsg = async (conn, msg) => {
  try {
    const sent = await brokerSend(conn, msg)
    console.log('sent')
    console.log('sucess')
  } catch (err) {
    console.log(err)
  }
}

// const msg = process.argv[3] || safeStringify({ ccc: 'blaaa' })
const values = {
  emailAdress: 'andre@gmail.com',
  name: 'andres',
  notInTable: 'not',
}

const msg = {
  client: 'testing',
  queryName: 'userCreate',
  values,
}
brokerConnAndSend('test', msg)
