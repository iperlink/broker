const { brokerConnect, brokerSend, brokerConnAndPub } = require('../index')
const safeStringify = require('fast-safe-stringify')


const connectAsync = async queue => {
  const conn = await brokerConnect(queue)
  return conn
}

const sendMsg = async (conn, msg) => {
  const sent = await brokerSend(conn, msg)
  try {
    console.log('sent')
    console.log('sucess')
  } catch (err) {
    console.log(err)
  }
}

const msg = safeStringify({ ccc: 'blaaa' })
brokerConnAndPub('pubTest', msg)
